import React, { Component } from 'react';
import MorseSignal from './MorseSignal';
import styles from './index.css';

class App extends Component {
  render() {
    return (
      <MorseSignal/>
    );
  }
}

export default App;
