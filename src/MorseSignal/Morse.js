function sleep(millisec) {
  return new Promise(resolve => setTimeout(resolve, millisec));
}

function calcTiming(wordsPerMinute) {
  const dot = //ms
    60000 /*ms/min*/ / 50 /*dot/word, see http://www.kent-engineers.com/codespeed.htm */ / wordsPerMinute /*word/min*/;

  return {
    dot: dot,
    dash: 3 * dot,
    symbolSpacing: dot, // symbol is a dot or a dash
    letterSpacing: 3 * dot, // between letters
    wordSpacing: 7 * dot // after the sequence
  };
}

class Morse {
  static ENCODE_TABLE = {
    a: '.-',
    b: '-...',
    c: '-.-.',
    d: '-..',
    e: '.',
    f: '..-.',
    g: '--.',
    h: '....',
    i: '..',
    j: '.---',
    k: '-.-',
    l: '.-..',
    m: '--',
    n: '-.',
    o: '---',
    p: '.--.',
    q: '--.-',
    r: '.-.',
    s: '...',
    t: '-',
    u: '..-',
    v: '...-',
    w: '.--',
    x: '-..-',
    y: '-.--',
    z: '--..'
  };
  static DECODE_TABLE = {
    '.-': 'a',
    '-...': 'b',
    '-.-.': 'c',
    '-..': 'd',
    '.': 'e',
    '..-.': 'f',
    '--.': 'g',
    '....': 'h',
    '..': 'i',
    '.---': 'j',
    '-.-': 'k',
    '.-..': 'l',
    '--': 'm',
    '-.': 'n',
    '---': 'o',
    '.--.': 'p',
    '--.-': 'q',
    '.-.': 'r',
    '...': 's',
    '-': 't',
    '..-': 'u',
    '...-': 'v',
    '.--': 'w',
    '-..-': 'x',
    '-.--': 'y',
    '--..': 'z'
  };

  // invokes on() and off() on handler
  static async play(handler, sequence, wordsPerMinute) {
    const timing = calcTiming(wordsPerMinute);
    const dot = async () => {
      handler.on();
      await sleep(timing.dot);
      handler.off();
      await sleep(timing.symbolSpacing);
    };
    const dash = async () => {
      handler.on();
      await sleep(timing.dash);
      handler.off();
      await sleep(timing.symbolSpacing);
    };
    const rest = async () => {
      await sleep(timing.letterSpacing);
    };

    for (let i = 0; i < sequence.length; ++i) {
      const symbol = sequence.charAt(i);
      switch (symbol) {
        case '.':
          await dot();
          break;
        case '-':
          await dash();
          break;
        case ' ':
          await rest();
          break;
        default:
          throw new Error('Unknown symbol');
      }
    }

    await sleep(timing.wordSpacing);
  }

  static encode(str) {
    return str
      .toLowerCase()
      .split('')
      .reduce((tmp, char) => {
        const encoded = Morse.ENCODE_TABLE[char];
        if (!encoded) throw new Error('Unknown symbol');
        return tmp ? tmp + ' ' + encoded : encoded;
      }, '');
  }

  static decode(encoded) {
    return encoded.split(' ').reduce((tmp, symbol) => {
      const decoded = Morse.DECODE_TABLE[symbol];
      if (!decoded) throw new Error('Unknown symbol');
      return tmp + decoded;
    }, '');
  }
}

export default Morse;
