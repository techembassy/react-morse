import React from 'react';
import Morse from './Morse';

class MorseSignal extends React.Component {
  static IMG_ON = '/images/morse/led-on.png';
  static IMG_OFF = '/images/morse/led-off.png';
  static WORDS_PER_MINUTE = 10;

  constructor(props) {
    super(props);
    this.led = React.createRef();
  }

  static getSequence() {
    // console.log(Morse.encode('letsencodethis'));
    console.log('.--. .-.. .- -.');
    return '.--. .-.. .- -.';
  }

  on() {
    this.led.current.src = MorseSignal.IMG_ON;
  }
  off() {
    this.led.current.src = MorseSignal.IMG_OFF;
  }

  async componentDidMount() {
    for (;;) await Morse.play(this, MorseSignal.getSequence(), MorseSignal.WORDS_PER_MINUTE);
  }

  render() {
    return (
      <div>
        <center>
          <img id="led" ref={this.led} width="256" height="256" alt="riddle" />
          <p>
            Words per minute: {MorseSignal.WORDS_PER_MINUTE}
            <br />
            International
          </p>
        </center>
      </div>
    );
  }
}

export default MorseSignal;
