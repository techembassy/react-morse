# Simple Morse React application

This project contains a React component called `MorseSignal` (see `src/MorseSignal/index.js`) which displays a blinking led with some labels.

## Detailed description
`Morse` class (see `src/MorseSignal/Morse.js`) has some useful methods like
- `encode(str)`: Encodes a word to morse symbols (e.g. `earth` -> `. .- .-. - ....`)
- `decode(encoded)`: Decodes an encoded word (e.g. `. .- .-. - ....` -> `earth`)
- `play(handler, sequence, wordsPerMinute)`: [JS async function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function) which plays the encoded `sequence` once, turns a led (or sound, ...) on and off using `handler`'s `on()` and `off()` method. See [http://www.kent-engineers.com/codespeed.htm](http://www.kent-engineers.com/codespeed.htm) for detailed description of `wordsPerMinute` parameter.

`MorseSignal` class (see `src/MorseSignal/index.js`) is responsible for displaying a light, which signals a secret message using Morse code. It uses `Morse` class' `play()` method.

All other files' purpose is to make the React app work, and make it contain a `MorseSignal` component.

## How to run
You have to make sure that [nodejs and npm](https://nodejs.org/en/download/) is installed on your computer
```sh
git clone https://gitlab.com/techembassy/react-morse.git
cd react-morse
npm install
npm start
```